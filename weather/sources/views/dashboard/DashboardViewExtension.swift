//
//  DashboardViewExtension.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 10/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

extension DashboardViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
    }
    
    func setContent() {
        guard let weather = viewModel.weather else { return }
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let stringDate = formatter.string(from: Date(timeIntervalSince1970: TimeInterval(weather.dt)))
        DispatchQueue.main.async {
            let temp = weather.main?.temp ?? 0.0
            let humidity = weather.main?.humidity ?? 0.0
            let pressure = weather.main?.pressure ?? 0.0
            let lat = weather.coordinate?.lat ?? 0.0
            let lon = weather.coordinate?.lon ?? 0.0
            let wind = weather.wind?.speed ?? 0.0
            self.currentLocationLabel.text = weather.name.capitalized
            self.dateLabel.text = stringDate
            self.temperatureLabel.text = [String(describing: temp.convertToCelcius()), "°C"].joined()
            self.humidityLabel.text = ["H ", String(describing: humidity), "  P ", String(describing: pressure), "  W ", String(describing: wind)].joined()
            self.latitudeLabel.text = "Lat \(lat) . Lon \(lon)"
            guard let `weather` = weather.weather.first else { return}
            self.weatherImage.setImage(WeatherName.render(weather.main))
            self.weatherDesciptionLabel.text = weather.description.capitalized
        }
    }
    
    
    private
    func setViews() {
        view.backgroundColor = .darkGray
        DispatchQueue.main.async {
            self.setLayouts()
        }
        self.navigationController?.hide()
        
        searchButton.addTarget(self, action: #selector(searchButtonTapped(_:)), for: .touchUpInside)
        forceCastButton.addTarget(self, action: #selector(foreCastButtonTapped(_:)), for: .touchUpInside)
        listButton.addTarget(self, action: #selector(listButtonTapped(_:)), for: .touchUpInside)
    }
    
    private
    func setLayouts() {
        view.addSubview(dateLabel)
        view.addSubview(weatherImage)
        view.addSubview(weatherDesciptionLabel)
        view.addSubview(currentLocationLabel)
        view.addSubview(latitudeLabel)
        view.addSubview(temperatureLabel)
        view.addSubview(humidityLabel)
        view.addSubview(forceCastButton)
        view.addSubview(searchButton)
        view.addSubview(listButton)
        
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        latitudeLabel.translatesAutoresizingMaskIntoConstraints = false
        currentLocationLabel.translatesAutoresizingMaskIntoConstraints = false
        temperatureLabel.translatesAutoresizingMaskIntoConstraints = false
        weatherImage.translatesAutoresizingMaskIntoConstraints = false
        weatherDesciptionLabel.translatesAutoresizingMaskIntoConstraints = false
        humidityLabel.translatesAutoresizingMaskIntoConstraints = false
        forceCastButton.translatesAutoresizingMaskIntoConstraints = false
        searchButton.translatesAutoresizingMaskIntoConstraints = false
        listButton.translatesAutoresizingMaskIntoConstraints = false
        
        dateLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 80).isActive = true
        dateLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        
        weatherImage.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 10).isActive = true
        weatherImage.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        weatherImage.heightAnchor.constraint(equalToConstant: 200).isActive = true
        weatherImage.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        currentLocationLabel.topAnchor.constraint(equalTo: weatherImage.bottomAnchor, constant: 10).isActive = true
        currentLocationLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        currentLocationLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15).isActive = true
        currentLocationLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        
        weatherDesciptionLabel.topAnchor.constraint(equalTo: currentLocationLabel.bottomAnchor, constant: 10).isActive = true
        weatherDesciptionLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        weatherDesciptionLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15).isActive = true
        weatherDesciptionLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        
        temperatureLabel.topAnchor.constraint(equalTo: weatherDesciptionLabel.bottomAnchor, constant: 80).isActive = true
        temperatureLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        
        humidityLabel.topAnchor.constraint(equalTo: temperatureLabel.bottomAnchor, constant: 5).isActive = true
        humidityLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        
        latitudeLabel.topAnchor.constraint(equalTo: humidityLabel.bottomAnchor, constant: 5).isActive = true
        latitudeLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        
        forceCastButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -5).isActive = true
        forceCastButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        forceCastButton.widthAnchor.constraint(equalToConstant: 130).isActive = true
        forceCastButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        listButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15).isActive = true
        listButton.centerYAnchor.constraint(equalTo: forceCastButton.centerYAnchor, constant: 0).isActive = true
        listButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        listButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        searchButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15).isActive = true
        searchButton.bottomAnchor.constraint(equalTo: listButton.topAnchor, constant: -15).isActive = true
        searchButton.centerXAnchor.constraint(equalTo: listButton.centerXAnchor, constant: 0).isActive = true
        searchButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        searchButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
}
