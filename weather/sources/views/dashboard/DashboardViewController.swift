//
//  DashboardViewController.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 10/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit
import CoreLocation

protocol DashboardConstructor {
    func setLocation()
    func showAddLocation()
}


class DashboardViewController: UIViewController {
    
    lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.text = "Time"
        label.textAlignment = .center
        label.textColor = .white
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        return label
    }()
    
    lazy var weatherImage: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.tintColor = .gray
        return view
    }()
    
    lazy var currentLocationLabel: UILabel = {
        let label = UILabel()
        label.text = "...Location"
        label.textAlignment = .center
        label.textColor = .white
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 38, weight: .heavy)
        return label
    }()
    
    lazy var weatherDesciptionLabel: UILabel = {
        let label = UILabel()
        label.text = "..."
        label.textAlignment = .center
        label.textColor = .white
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        return label
    }()
    
    lazy var latitudeLabel: UILabel = {
        let label = UILabel()
        label.text = "Lat: ..."
        label.textAlignment = .left
        label.textColor = .white
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12, weight: .light)
        return label
    }()
    
    lazy var weatherLabel: UILabel = {
        let label = UILabel()
        label.text = "weather..."
        label.textAlignment = .left
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 24, weight: .heavy)
        return label
    }()
    
    lazy var temperatureLabel: UILabel = {
        let label = UILabel()
        label.text = "...°C"
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 60, weight: .light)
        return label
    }()
    
    lazy var humidityLabel: UILabel = {
        let label = UILabel()
        label.text = "...°C"
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        return label
    }()
    
    lazy var forceCastButton: UIButton = {
        let button = UIButton()
        let image = UIImage(named: "forecast")
        image?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.setTitle(" Forecast", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        button.contentHorizontalAlignment = .left
        button.tintColor = .white
        return button
    }()
    
    lazy var searchButton: UIButton = {
        let button = UIButton()
        let image = UIImage(named: "search")
        image?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = .white
        return button
    }()
    
    lazy var listButton: UIButton = {
        let button = UIButton()
        let image = UIImage(named: "storage")
        image?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = .white
        return button
    }()
    
    var locationManager = CLLocationManager()
    var currentLoc: CLLocation?
    var viewModel: DashboardViewModel!
    
    init(_ viewModel: DashboardViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
        self.viewModel.interactor = self
        self.viewModel.dashboardInteractor = self
        setLocation()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    @objc func searchButtonTapped(_ sender: UIButton) {
        showAddLocation()
    }
    
    @objc func foreCastButtonTapped(_ sender: UIButton) {
        if Reachability.shared.isNetworkAvailable() {
            let viewModel = ForeCastViewModel()
            viewModel.weather = self.viewModel.weather
            let viewController = ForeCastViewController(viewModel)
            
            present(viewController, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "Network not available", message: "", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action : UIAlertAction!) -> Void in
                print("Cancel")
            })
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func listButtonTapped(_ sender: UIButton) {
        let viewModel = CityViewModel()
        let viewController = CitiesViewController(viewModel)
        viewController.interactor = self
        present(viewController, animated: true, completion: nil)
    }
}

extension DashboardViewController: DashboardConstructor {
    func setLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func showAddLocation() {
        var alertController = UIAlertController()
        if Reachability.shared.isNetworkAvailable() {
            alertController = UIAlertController(title: "Add City", message: "", preferredStyle: .alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
               textField.placeholder = "City Name"
            }

            let saveAction = UIAlertAction(title: "Add", style: .default, handler: { alert -> Void in
                let firstTextField = alertController.textFields![0] as UITextField
                print("City Name: \(String(describing: firstTextField.text))")
                guard let cityname = firstTextField.text else { return }
                self.viewModel.getCityWeather(city: cityname)
            })

            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action : UIAlertAction!) -> Void in
                print("Cancel")
            })

            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
        } else {
            alertController = UIAlertController(title: "Network not available", message: "", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action : UIAlertAction!) -> Void in
                print("Cancel")
            })
            alertController.addAction(cancelAction)
        }
        

        self.present(alertController, animated: true, completion: nil)
    }
}



extension DashboardViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        manager.delegate = nil
        let location = locations[0].coordinate
        DispatchQueue.main.async {
            if Reachability.shared.isNetworkAvailable() {
                self.viewModel.getCurrentWeather(lat: location.latitude.description, lon: location.longitude.description)
            } else {
                self.viewModel.getLocalData()
            }
        }
    }
}


// MARK: - ViewModelInteractor
extension DashboardViewController: ViewModelInteractor {
    func success(_ network: Network) {
        setContent()
    }
    
    func failed(_ message: String) {
        debugPrint(message)
    }
}

// MARK: - DashboardViewModelInteractor
extension DashboardViewController: DashboardViewModelInteractor {
    func reloadData() {
        setContent()
    }
}

// MARK: - CitiesViewControllerInteractor
extension DashboardViewController: CitiesViewControllerInteractor {
    func selected(_ object: WeatherData) {
        viewModel.weather = object
        setContent()
    }
}
