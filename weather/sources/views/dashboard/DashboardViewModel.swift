//
//  DashboardViewModel.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 10/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

protocol DashboardViewModelConstructor {
    var weather: WeatherData? { get set }
    func getLocalData()
}

protocol DashboardViewModelInteractor {
    func reloadData()
}

class DashboardViewModel: Domain, DashboardViewModelConstructor {
    var weather: WeatherData?
    var networkService: NetworkService = NetworkService()
    var interactor: ViewModelInteractor?
    var dashboardInteractor: DashboardViewModelInteractor?
    
    init() {
        self.networkService.interactor = self
    }
    
    func getLocalData() {
        let objects = Storage.fetch(WeatherData.self)
        self.weather = objects.last
        dashboardInteractor?.reloadData()
    }
}

extension DashboardViewModel: NetworkServiceInteractor {
    func success(_ object: Codable, network: Network) {
        guard let `object` = object as? WeatherData else {
            interactor?.failed("Failed populate data")
            return
        }
        Storage.save(object, name: [object.name.lowercased().replacingOccurrences(of: " ", with: "-"), "-weather"].joined())
        self.weather = object
        interactor?.success(network)
    }
    
    func failed(_ message: String, network: Network) {
        interactor?.failed(message)
    }
}
