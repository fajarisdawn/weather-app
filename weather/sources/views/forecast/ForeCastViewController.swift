//
//  ForeCastViewController.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 11/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import UIKit

class ForeCastViewController: UIViewController {
    lazy var collectionView : UICollectionView = {
        let view = UICollectionView(frame: .zero, collectionViewLayout: createCompositionalLayout())
        return view
    }()
    
    var viewModel: ForeCastViewModel!
    
    init(_ viewModel: ForeCastViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
        self.viewModel.interactor = self
        guard let weather = viewModel.weather else { return }
        self.viewModel.getForecast(city: weather.name)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

extension ForeCastViewController: ViewModelInteractor {
    func success(_ network: Network) {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func failed(_ message: String) {
        
    }
}
