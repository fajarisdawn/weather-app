//
//  ForeCastViewModel.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 11/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

protocol ForeCastViewModelConstructor {
    var weather: WeatherData? { get set }
    var foreCasts: [ForecastTemperature] { get set }
}


class ForeCastViewModel: Domain, ForeCastViewModelConstructor {
    var networkService: NetworkService = NetworkService()
    var weather: WeatherData?
    var foreCasts: [ForecastTemperature] = []
    var interactor: ViewModelInteractor?
    
    init() {
        networkService.interactor = self
    }
}

extension ForeCastViewModel: NetworkServiceInteractor {
    func success(_ object: Codable, network: Network) {
        guard let objects = object as? ForecastData else { return }
        var currentDayTemp = ForecastTemperature(weekDay: nil, hourlyForecast: nil)
        var secondDayTemp = ForecastTemperature(weekDay: nil, hourlyForecast: nil)
        var thirdDayTemp = ForecastTemperature(weekDay: nil, hourlyForecast: nil)
        var fourthDayTemp = ForecastTemperature(weekDay: nil, hourlyForecast: nil)
        var fifthDayTemp = ForecastTemperature(weekDay: nil, hourlyForecast: nil)
        var sixthDayTemp = ForecastTemperature(weekDay: nil, hourlyForecast: nil)
        
        var forecastmodelArray : [ForecastTemperature] = []
        var fetchedData : [WeatherInfo] = [] //Just for loop completion
        var currentDayForecast : [WeatherInfo] = []
        var secondDayForecast : [WeatherInfo] = []
        var thirddayDayForecast : [WeatherInfo] = []
        var fourthDayDayForecast : [WeatherInfo] = []
        var fifthDayForecast : [WeatherInfo] = []
        var sixthDayForecast : [WeatherInfo] = []
        
        var totalData = objects.list.count
        
        for day in 0...objects.list.count - 1 {
        
        
            let listIndex = day//(8 * day) - 1
            let mainTemp = objects.list[listIndex].main?.temp ?? 0.0
            let minTemp = objects.list[listIndex].main?.tempMin ?? 0.0
            let maxTemp = objects.list[listIndex].main?.tempMax ?? 0.0
            let descriptionTemp = objects.list[listIndex].weather[0].description
            let icon = objects.list[listIndex].weather[0].icon
            let time = objects.list[listIndex].dtTxt
            
            let dateFormatter = DateFormatter()
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: objects.list[listIndex].dtTxt)
            
            let calendar = Calendar.current
            let components = calendar.dateComponents([.weekday], from: date!)
            let weekdaycomponent = components.weekday! - 1  //Just the integer value from 0 to 6
            
            let f = DateFormatter()
            let weekday = f.weekdaySymbols[weekdaycomponent] // 0 Sunday 6 - Saturday //This is where we are getting the string val (Mon/Tue/Wed...)
            
            let currentDayComponent = calendar.dateComponents([.weekday], from: Date())
            let currentWeekDay = currentDayComponent.weekday! - 1
            let currentweekdaysymbol = f.weekdaySymbols[currentWeekDay]
            
            if weekdaycomponent == currentWeekDay - 1 {
                totalData = totalData - 1
            }
            
            if weekdaycomponent == currentWeekDay {
                let info = WeatherInfo(temp: mainTemp, minTemp: minTemp, maxTemp: maxTemp, description: descriptionTemp, icon: icon, time: time)
                currentDayForecast.append(info)
                currentDayTemp = ForecastTemperature(weekDay: currentweekdaysymbol, hourlyForecast: currentDayForecast)
                print("1")
                fetchedData.append(info)
            } else if weekdaycomponent == currentWeekDay.incrementWeekDays(by: 1) {
                let info = WeatherInfo(temp: mainTemp, minTemp: minTemp, maxTemp: maxTemp, description: descriptionTemp, icon: icon, time: time)
                secondDayForecast.append(info)
                secondDayTemp = ForecastTemperature(weekDay: weekday, hourlyForecast: secondDayForecast)
                print("2")
                fetchedData.append(info)
            } else if weekdaycomponent == currentWeekDay.incrementWeekDays(by: 2) {
                let info = WeatherInfo(temp: mainTemp, minTemp: minTemp, maxTemp: maxTemp, description: descriptionTemp, icon: icon, time: time)
                thirddayDayForecast.append(info)
                print("3")
                thirdDayTemp = ForecastTemperature(weekDay: weekday, hourlyForecast: thirddayDayForecast)
                fetchedData.append(info)
            } else if weekdaycomponent == currentWeekDay.incrementWeekDays(by: 3) {
                let info = WeatherInfo(temp: mainTemp, minTemp: minTemp, maxTemp: maxTemp, description: descriptionTemp, icon: icon, time: time)
                fourthDayDayForecast.append(info)
                print("4")
                fourthDayTemp = ForecastTemperature(weekDay: weekday, hourlyForecast: fourthDayDayForecast)
                fetchedData.append(info)
            } else if weekdaycomponent == currentWeekDay.incrementWeekDays(by: 4){
                let info = WeatherInfo(temp: mainTemp, minTemp: minTemp, maxTemp: maxTemp, description: descriptionTemp, icon: icon, time: time)
                fifthDayForecast.append(info)
                fifthDayTemp = ForecastTemperature(weekDay: weekday, hourlyForecast: fifthDayForecast)
                fetchedData.append(info)
                print("5")
            } else if weekdaycomponent == currentWeekDay.incrementWeekDays(by: 5) {
                let info = WeatherInfo(temp: mainTemp, minTemp: minTemp, maxTemp: maxTemp, description: descriptionTemp, icon: icon, time: time)
                sixthDayForecast.append(info)
                sixthDayTemp = ForecastTemperature(weekDay: weekday, hourlyForecast: sixthDayForecast)
                fetchedData.append(info)
                print("6")
            }
        }
        
        if fetchedData.count == totalData {
            
            if currentDayTemp.hourlyForecast?.count ?? 0 > 0 {
                forecastmodelArray.append(currentDayTemp)
            }
            
            if secondDayTemp.hourlyForecast?.count ?? 0 > 0 {
                forecastmodelArray.append(secondDayTemp)
            }
            
            if thirdDayTemp.hourlyForecast?.count ?? 0 > 0 {
                forecastmodelArray.append(thirdDayTemp)
            }
            
            if fourthDayTemp.hourlyForecast?.count ?? 0 > 0 {
                forecastmodelArray.append(fourthDayTemp)
            }
            
            if fifthDayTemp.hourlyForecast?.count ?? 0 > 0 {
                forecastmodelArray.append(fifthDayTemp)
            }
            
            if sixthDayTemp.hourlyForecast?.count ?? 0 > 0 {
                forecastmodelArray.append(sixthDayTemp)
            }
            
            if forecastmodelArray.count <= 6 {
                self.foreCasts = forecastmodelArray
                interactor?.success(network)
            }
            
        }
        
    }
    
    func failed(_ message: String, network: Network) {
        interactor?.failed(message)
    }
}
