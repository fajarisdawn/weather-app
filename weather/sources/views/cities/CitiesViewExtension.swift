//
//  CitiesViewExtension.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 11/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

extension CitiesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
    }
    
    private
    func setViews() {
        tableView.registerReusableCell(CityCell.self)
        tableView.backgroundColor = .darkGray
        DispatchQueue.main.async {
            self.setLayouts()
        }
    }
    
    private
    func setLayouts() {
        view.addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
    }
}
