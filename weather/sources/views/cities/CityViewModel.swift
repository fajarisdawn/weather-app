//
//  CityViewModel.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 11/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

protocol CityViewModelConstructor {
    var weathers: [WeatherData] { get set }
}

class CityViewModel: CityViewModelConstructor {
    var weathers: [WeatherData] = []
    var interactor: ViewModelInteractor?
    
    init() {
        self.weathers = Storage.fetch(WeatherData.self).reversed()
    }
}
