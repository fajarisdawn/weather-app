//
//  Weather.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 10/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

struct WeatherData: Codable {
    var coordinate: Coordinate?
    var wind: Wind?
    var weather: [Weather]
    var main: Main?
    var sys: Sys?
    var name: String
    var dt: Int
    var timezone: Int
    var dtTxt: String
    
    enum CodingKeys: String, CodingKey {
        case weather, main, sys, name, dt, timezone, wind
        case dtTxt = "dt_txt"
        case coordinate = "coord"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.coordinate = try container.decodeIfPresent(Coordinate.self, forKey: .coordinate) ?? nil
        self.weather = try container.decodeIfPresent(Array<Weather>.self, forKey: .weather) ?? []
        self.main = try container.decodeIfPresent(Main.self, forKey: .main) ?? nil
        self.sys = try container.decodeIfPresent(Sys.self, forKey: .sys) ?? nil
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.dt = try container.decodeIfPresent(Int.self, forKey: .dt) ?? 0
        self.timezone = try container.decodeIfPresent(Int.self, forKey: .timezone) ?? 0
        self.dtTxt = try container.decodeIfPresent(String.self, forKey: .dtTxt) ?? ""
        self.wind = try container.decodeIfPresent(Wind.self, forKey: .wind) ?? nil
    }
}



struct Weather: Codable {
    let id: Int
    let main: String
    let description: String
    let icon: String
    
    enum CodingKeys: String, CodingKey {
        case id, main, description, icon
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.main = try container.decodeIfPresent(String.self, forKey: .main) ?? ""
        self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        self.icon = try container.decodeIfPresent(String.self, forKey: .icon) ?? ""
    }
}
