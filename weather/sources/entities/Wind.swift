//
//  Wind.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 11/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

struct Wind: Codable {
    var speed: Float
    var deg: Float
    var gust: Float
    
    enum CodingKeys: String, CodingKey {
        case speed, deg, gust
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.speed = try container.decodeIfPresent(Float.self, forKey: .speed) ?? 0.0
        self.deg = try container.decodeIfPresent(Float.self, forKey: .deg) ?? 0.0
        self.gust = try container.decodeIfPresent(Float.self, forKey: .gust) ?? 0.0
    }
}
