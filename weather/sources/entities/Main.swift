//
//  Main.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 11/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

struct Main: Codable {
    var temp: Float
    var feelsLike: Float
    var tempMin: Float
    var tempMax: Float
    var pressure: Float
    var humidity: Float
    
    enum CodingKeys: String, CodingKey {
        case temp, pressure, humidity
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.temp = try container.decodeIfPresent(Float.self, forKey: .temp) ?? 0.0
        self.feelsLike = try container.decodeIfPresent(Float.self, forKey: .feelsLike) ?? 0.0
        self.tempMin = try container.decodeIfPresent(Float.self, forKey: .tempMin) ?? 0.0
        self.tempMax = try container.decodeIfPresent(Float.self, forKey: .tempMax) ?? 0.0
        self.pressure = try container.decodeIfPresent(Float.self, forKey: .pressure) ?? 0.0
        self.humidity = try container.decodeIfPresent(Float.self, forKey: .humidity) ?? 0.0
    }
}
