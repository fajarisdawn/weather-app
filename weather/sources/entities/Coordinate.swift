//
//  Coordinate.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 11/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

struct Coordinate: Codable {
    var lat: Float
    var lon: Float
    
    enum CodingKeys: String, CodingKey {
        case lat, lon
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.lat = try container.decodeIfPresent(Float.self, forKey: .lat) ?? 0.0
        self.lon = try container.decodeIfPresent(Float.self, forKey: .lon) ?? 0.0
    }
}
