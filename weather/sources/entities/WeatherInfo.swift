//
//  WeatherInfo.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 11/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

struct WeatherInfo: Codable {
    var temp: Float
    var minTemp: Float
    var maxTemp: Float
    var description: String
    var icon: String
    var time: String
    
//    enum CodingKeys: String, CodingKey {
//        case temp, description, icon, time
//        case minTemp = "min_temp"
//        case maxTemp = "max_temp"
//    }
//    
//    public init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.temp = try container.decodeIfPresent(Float.self, forKey: .temp) ?? 0.0
//        self.minTemp = try container.decodeIfPresent(Float.self, forKey: .minTemp) ?? 0.0
//        self.maxTemp = try container.decodeIfPresent(Float.self, forKey: .maxTemp) ?? 0.0
//        self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
//        self.icon = try container.decodeIfPresent(String.self, forKey: .icon) ?? ""
//        self.time = try container.decodeIfPresent(String.self, forKey: .time) ?? ""
//    }
}
