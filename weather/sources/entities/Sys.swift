//
//  Sys.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 11/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

struct Sys: Codable {
    var country: String
    var sunrise: Int
    var sunset: Int
    
    enum CodingKeys: String, CodingKey {
        case country, sunrise, sunset
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.country = try container.decodeIfPresent(String.self, forKey: .country) ?? ""
        self.sunrise = try container.decodeIfPresent(Int.self, forKey: .sunrise) ?? 0
        self.sunset = try container.decodeIfPresent(Int.self, forKey: .sunset) ?? 0
    }
}
