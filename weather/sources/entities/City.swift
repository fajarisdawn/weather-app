//
//  City.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 11/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

struct City: Codable {
    var name: String
    var country: String
    
    enum CodingKeys: String, CodingKey {
        case name, country
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.country = try container.decodeIfPresent(String.self, forKey: .country) ?? ""
    }
}
