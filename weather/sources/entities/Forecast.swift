//
//  Forecast.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 10/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

struct ForecastTemperature: Codable {
    var weekDay: String?
    var hourlyForecast: [WeatherInfo]?
    
//    enum CodingKeys: String, CodingKey {
//        case weekDay, hourlyForecast
//    }
//    
//    public init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.weekDay = try container.decodeIfPresent(String.self, forKey: .weekDay) ?? ""
//        self.hourlyForecast = try container.decodeIfPresent(Array<WeatherInfo>.self, forKey: .hourlyForecast) ?? []
//    }
}

struct ForecastData: Codable {
    var list: [WeatherData]
    var city: City?
    
    enum CodingKeys: String, CodingKey {
        case list, city
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.list = try container.decodeIfPresent(Array<WeatherData>.self, forKey: .list) ?? []
        self.city = try container.decodeIfPresent(City.self, forKey: .city) ?? nil
    }
}
