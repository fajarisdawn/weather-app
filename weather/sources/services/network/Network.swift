//
//  Network.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 10/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

protocol EndpointType {
    var baseUrl: URL { get }
    var path: String { get }
}

enum Network {
    case none
    case getCityWeather(city: String)
    case getCurrentWeather(lat: String, lon: String)
    case getForecast(city: String)
}

extension Network: EndpointType {
    var baseUrl: URL {
        return URL(string: "http://api.openweathermap.org/data/2.5/")!
    }
    
    var path: String {
        switch self {
        case .getCityWeather(let city):
            let `city` = city.replacingOccurrences(of: " ", with: "+")
            return "weather?q=\(city)&appid=\(NetworkProperties.appId)"
        case .getCurrentWeather(let lat, let lon):
            return "weather?lat=\(lat)&lon=\(lon)&appid=\(NetworkProperties.appId)"
        case .getForecast(let city):
            let `city` = city.replacingOccurrences(of: " ", with: "+")
            return "forecast?q=\(city.lowercased())&appid=\(NetworkProperties.appId)"
        default:
            return ""
        }
    }
}
