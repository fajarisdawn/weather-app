//
//  Domain.swift
//  weather
//
//  Created by Fajar Adiwa Sentosa on 10/06/22.
//  Copyright © 2022 Fajar Adiwa Sentosa. All rights reserved.
//

import Foundation

protocol Domain {
    var networkService: NetworkService { get set }
    func getCityWeather(city: String)
    func getCurrentWeather(lat: String, lon: String)
    func getForecast(city: String)
}

extension Domain {
    func getCityWeather(city: String) {
        networkService.task(network: .getCityWeather(city: city), type: WeatherData.self)
    }
    
    func getCurrentWeather(lat: String, lon: String) {
        networkService.task(network: .getCurrentWeather(lat: lat, lon: lon), type: WeatherData.self)
    }
    
    func getForecast(city: String) {
        networkService.task(network: .getForecast(city: city), type: ForecastData.self)
    }
}


